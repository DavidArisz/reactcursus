import { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer, useTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Constants from 'expo-constants'
import { Home, Detail, Form, ShoppingCart} from "./components/pages"
import { NativeBaseProvider } from "native-base";
import AppContext from './context/AppContext'
import { ShoppingCartButton } from './components/atoms';
import { HomeButton } from './components/atoms';
import useFetch from './hooks/useFetch'
import config from './config/config';

/// Add a stack
const Stack = createStackNavigator();

const App = () => {

  const [shoppingCart, setShoppingCart] = useState([])

  const {data,isLoaded} = useFetch(config.serverUrl)

  const [image, setImage] = useState('')

  const updateShoppingCart = (item) => {
    setShoppingCart( cart => [...cart, item ])
  }

  const updateImage = (newUrl) => {
    setImage( url = newUrl)
  }

  return (
    <NativeBaseProvider>
    <AppContext.Provider value={{ data, isLoaded, shoppingCart, updateShoppingCart, image, updateImage }}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Home'
                          screenOptions={{
                            headerShown: true,
                            headerLeft: () => <HomeButton />,
                            headerRight: () => <ShoppingCartButton />
                            }} >
            <Stack.Screen name="Homepage" component={ Home }/>
            <Stack.Screen name="Detail" component={ Detail }/>
            <Stack.Screen name="Form" component={ Form }/>
            <Stack.Screen name="ShoppingCart" component={ ShoppingCart }/>
        </Stack.Navigator>
      </NavigationContainer>
    </AppContext.Provider>
    </NativeBaseProvider>
  )
}

let AppEntryPoint = App

if (Constants.expoConfig?.extra?.storybookEnabled === 'true') {
  AppEntryPoint = require('./.ondevice').default
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default AppEntryPoint
