import { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import ButtonStyle from './Button.style'

/// TODO: Define & Destructure props
const Button = ({ text, action }) => {

    return(
        <TouchableOpacity 
            onPress={ action }
              style={ ButtonStyle.View }>
            <Text>{ text }</Text>
        </TouchableOpacity>
    )

}

/// TODO: add more utility classes if needed!

/// TODO: Adjust and extend!
const sizes = [
    "small",
    "medium",
    "large"
]

const exceptionClasses = [
    "button",
    "regular"
]

Button.propTypes = {
    testID: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(exceptionClasses),
    size: PropTypes.oneOf(sizes),
}

export default Button