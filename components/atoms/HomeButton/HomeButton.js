import { useState, useEffect } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import HomeButtonStyle from './HomeButton.style'
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'

/// TODO: Define & Destructure props
const HomeButton = (props) => {

    const nav = useNavigation()

    return(
        <TouchableOpacity onPress={ () => nav.navigate('Homepage')}
              style={ HomeButtonStyle.View }>
            <Text>🏠</Text>
        </TouchableOpacity>
    )

}

/// TODO: add more utility classes if needed!

/// TODO: Adjust and extend!
const sizes = [
    "small",
    "medium",
    "large"
]

const exceptionClasses = [
    "button",
    "regular"
]

HomeButton.propTypes = {
    testID: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(exceptionClasses),
    size: PropTypes.oneOf(sizes),
}

export default HomeButton