import React from 'react'
import HomeButton from './HomeButton'

const HomeButtonMeta = {
    title: "atoms/HomeButton",
    component: HomeButton,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "HomeButton-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <HomeButton {...args} />

export const DefaultHomeButton = Template.bind({})
DefaultHomeButton.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default HomeButtonMeta