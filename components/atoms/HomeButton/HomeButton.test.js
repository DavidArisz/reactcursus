import React from 'react'
import { render, screen } from '@testing-library/react-native';


/// Preferably each story instead of actual component
/// TODO: fix test based upon story entries
import { DefaultHomeButton } from './HomeButton.stories'

const testID = "HomeButton-" + Math.floor(Math.random()*90000) + 10000

describe("HomeButton", () => {

    it("Can render DefaultHomeButton", () => {
        render(<DefaultHomeButton testID={ testID } />)
        let defaultCreated = screen.getByTestId(testID)
        expect(defaultCreated).not.toBeNull()
    })

})