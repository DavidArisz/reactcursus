import { useState, useEffect } from 'react'
import { View, Text } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import InputFieldStyle from './InputField.style'

/// TODO: Define & Destructure props
const InputField = ({ label, value, handler}) => {

    return(
        <View style={ InputFieldStyle.View }>
            <Text>{ label }</Text>
            <TextInput style={ InputFieldStyle.Input}
                        value={ value } 
                        onChangeText={handler} />
        </View>
    )

}

export default InputField