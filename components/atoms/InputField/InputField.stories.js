import React from 'react'
import InputField from './InputField'

const InputFieldMeta = {
    title: "atoms/InputField",
    component: InputField,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "InputField-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <InputField {...args} />

export const DefaultInputField = Template.bind({})
DefaultInputField.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default InputFieldMeta