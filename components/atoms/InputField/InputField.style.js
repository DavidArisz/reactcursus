/// TODO: add general style(s)
/// import Style from './path/to/general/style

const InputFieldStyle = {
    View: {
        padding: 10
    },
    Input: {
        padding: 5,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        backgroundColor: '#fff'
    }
}

export default InputFieldStyle