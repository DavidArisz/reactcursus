import React from 'react'
import { render, screen } from '@testing-library/react-native';


/// Preferably each story instead of actual component
/// TODO: fix test based upon story entries
import { DefaultInputField } from './InputField.stories'

const testID = "InputField-" + Math.floor(Math.random()*90000) + 10000

describe("InputField", () => {

    it("Can render DefaultInputField", () => {
        render(<DefaultInputField testID={ testID } />)
        let defaultCreated = screen.getByTestId(testID)
        expect(defaultCreated).not.toBeNull()
    })

})