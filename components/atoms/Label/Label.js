import { useState, useEffect } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import LabelStyle from './Label.style'

/// TODO: Define & Destructure props
const Label = (props) => {

    return(
        <View testID={ props.testID }
              style={ LabelStyle.View }>
            <Text>{ props.text }</Text>
        </View>
    )

}

/// TODO: add more utility classes if needed!

/// TODO: Adjust and extend!
const sizes = [
    "small",
    "medium",
    "large"
]

const exceptionClasses = [
    "button",
    "regular"
]

Label.propTypes = {
    testID: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(exceptionClasses),
    size: PropTypes.oneOf(sizes),
}

export default Label