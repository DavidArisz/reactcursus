import React from 'react'
import Label from './Label'

const LabelMeta = {
    title: "atoms/Label",
    component: Label,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "Label-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <Label {...args} />

export const DefaultLabel = Template.bind({})
DefaultLabel.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default LabelMeta