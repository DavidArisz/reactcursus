import { useState, useEffect, useContext } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import ShoppingCartButtonStyle from './ShoppingCartButton.style'

import AppContext from '../../../context/AppContext'
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'

/// TODO: Define & Destructure props
const ShoppingCartButton = ({ navigation }) => {

    const ctx = useContext(AppContext)
    const numItemsInCart = ctx.shoppingCart.length
    const nav = useNavigation()

    return(
        <TouchableOpacity onPress={ () => nav.navigate('ShoppingCart')}
              style={ ShoppingCartButtonStyle.View }>
            <Text>{numItemsInCart} 🛒</Text>
        </TouchableOpacity>
    )

}

export default ShoppingCartButton