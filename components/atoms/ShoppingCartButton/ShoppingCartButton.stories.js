import React from 'react'
import ShoppingCartButton from './ShoppingCartButton'

const ShoppingCartButtonMeta = {
    title: "atoms/ShoppingCartButton",
    component: ShoppingCartButton,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "ShoppingCartButton-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <ShoppingCartButton {...args} />

export const DefaultShoppingCartButton = Template.bind({})
DefaultShoppingCartButton.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default ShoppingCartButtonMeta