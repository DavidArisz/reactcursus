/// TODO: add general style(s)
/// import Style from './path/to/general/style

const ShoppingCartButtonStyle = {
    View: {
        width: 30,
        height: 30,
        backgroundColor: 'orange',
        margin: 10,
        borderRadius: 15,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
}

export default ShoppingCartButtonStyle