import React from 'react'
import { render, screen } from '@testing-library/react-native';


/// Preferably each story instead of actual component
/// TODO: fix test based upon story entries
import { DefaultShoppingCartButton } from './ShoppingCartButton.stories'

const testID = "ShoppingCartButton-" + Math.floor(Math.random()*90000) + 10000

describe("ShoppingCartButton", () => {

    it("Can render DefaultShoppingCartButton", () => {
        render(<DefaultShoppingCartButton testID={ testID } />)
        let defaultCreated = screen.getByTestId(testID)
        expect(defaultCreated).not.toBeNull()
    })

})