import { useContext } from 'react'
import { Image, TouchableOpacity } from 'react-native'
import ListImageStyle from './ListImage.style'
import AppContext from '../../../context/AppContext'

/// TODO: Define & Destructure props
const ListImage = ({image}) => {

    const ctx = useContext(AppContext)

    return(
        <TouchableOpacity onPress={() => {ctx.updateImage(image)}}>
            <Image source={{ uri: image}}
                            style = {ListImageStyle.ExtraImage.Image} />
        </TouchableOpacity>
    )
}

export default ListImage