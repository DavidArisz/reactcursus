import { useState, useEffect } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import ListItemStyle from './ListItem.style'
import { useNavigation } from '@react-navigation/native'

/// TODO: Define & Destructure props
const ListItem = ({item}) => {

    const nav = useNavigation()

    return(
        <TouchableOpacity 
        onPress={ () => nav.navigate('Detail', { item: item })}
        style={ ListItemStyle.View }>
            <View style={ ListItemStyle.Image} >
                <Image source={{ uri: item.thumbnail}}
                                style = {ListItemStyle.Image.Image} />
            </View>

            <View style={ ListItemStyle.Description}>
                <Text style = { ListItemStyle.Description.Title}>{ item.title }</Text>
                <Text style= {ListItemStyle.Description.Text}>{ item.description }</Text>
            </View>

            <View style = {ListItemStyle.Price}>
                <Text style={ ListItemStyle.Price.Text }>{ item.price }</Text>
            </View>
            
        </TouchableOpacity>
    )

}

/// TODO: add more utility classes if needed!

/// TODO: Adjust and extend!
const sizes = [
    "small",
    "medium",
    "large"
]

const exceptionClasses = [
    "button",
    "regular"
]

ListItem.propTypes = {
    testID: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(exceptionClasses),
    size: PropTypes.oneOf(sizes),
}

export default ListItem