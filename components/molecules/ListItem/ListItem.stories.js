import React from 'react'
import ListItem from './ListItem'

const ListItemMeta = {
    title: "atoms/ListItem",
    component: ListItem,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "ListItem-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <ListItem {...args} />

export const DefaultListItem = Template.bind({})
DefaultListItem.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default ListItemMeta