/// TODO: add general style(s)
/// import Style from './path/to/general/style

const ListItemStyle = {
    View: {
        padding: 10,
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#DDD",
        marginBottom: 10,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    Image: {
        flex: 1,
        Image: {
            width: 40,
            height: 40,
            resizeMode: "cover",
        }
    },
    Description: {
        flex: 3,
        Title: {
            fontWeight: "bold",
        },
        Text: {
            fontSize: 10,
        }
    },
    Price: {
        flex: 1,
        Text: {
            textAlign: "right",
            fontSize: 20,
        }
    }
}

export default ListItemStyle