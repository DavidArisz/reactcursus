import React from 'react'
import { render, screen } from '@testing-library/react-native';


/// Preferably each story instead of actual component
/// TODO: fix test based upon story entries
import { DefaultListItem } from './ListItem.stories'

const testID = "ListItem-" + Math.floor(Math.random()*90000) + 10000

describe("ListItem", () => {

    it("Can render DefaultListItem", () => {
        render(<DefaultListItem testID={ testID } />)
        let defaultCreated = screen.getByTestId(testID)
        expect(defaultCreated).not.toBeNull()
    })

})