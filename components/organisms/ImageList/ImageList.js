import { HStack, ScrollView } from "native-base";
import ListImage from '../../molecules/ListImage/ListImage';

/// TODO: Define & Destructure props
const ImageList = ( imageUrls ) => {

    const originalList = ['a', 'b', 'c']

    return(
        <ScrollView horizontal={true} _contentContainerStyle={ w="100%"}>
            <HStack w="100%" space={1} justifyContent={'space-between'}>
                {imageUrls.item.map((image) => {
                    return (
                        <ListImage image={image} />
                    )
                })}
            </HStack>
        </ScrollView>
    )
}

export default ImageList