import { useState, useEffect, useContext } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import ProductListStyle from './ProductList.style'

import AppContext from '../../../context/AppContext'
import { FlatList } from 'react-native-gesture-handler'
import { ListItem } from '../../molecules'

/// TODO: Define & Destructure props
const ProductList = ({ type }) => {

    const ctx = useContext(AppContext)
    const list = type === 'product' ? ctx.data : ctx.shoppingCart

    return(
        <View style={ ProductListStyle.View }>
            <FlatList
                data={list}
                renderItem={({item}) => <ListItem item = {item}></ListItem>}
                keyExtractor={(item, index) => index}
            />
        </View>
    )

}

export default ProductList