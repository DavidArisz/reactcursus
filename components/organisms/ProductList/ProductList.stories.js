import React from 'react'
import ProductList from './ProductList'

const ProductListMeta = {
    title: "atoms/ProductList",
    component: ProductList,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "ProductList-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <ProductList {...args} />

export const DefaultProductList = Template.bind({})
DefaultProductList.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default ProductListMeta