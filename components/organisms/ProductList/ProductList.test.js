import React from 'react'
import { render, screen } from '@testing-library/react-native';


/// Preferably each story instead of actual component
/// TODO: fix test based upon story entries
import { DefaultProductList } from './ProductList.stories'

const testID = "ProductList-" + Math.floor(Math.random()*90000) + 10000

describe("ProductList", () => {

    it("Can render DefaultProductList", () => {
        render(<DefaultProductList testID={ testID } />)
        let defaultCreated = screen.getByTestId(testID)
        expect(defaultCreated).not.toBeNull()
    })

})