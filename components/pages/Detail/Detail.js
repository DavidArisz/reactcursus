import { useState, useEffect, useContext } from 'react'
import {Image } from 'react-native'
import DetailStyle from './Detail.style'
import { Button } from '../../atoms'
import AppContext from '../../../context/AppContext'
import { VStack, Center, Box, Text } from 'native-base'
import ImageList from '../../organisms/ImageList/ImageList';

/// TODO: Define & Destructure props
const Detail = ({ navigation, route }) => {

    const item = route.params.item
    const ctx = useContext(AppContext)

    const updateCart = () => {
        ctx.updateShoppingCart(item)
    }

    useEffect(() => {
        ctx.updateImage(item.thumbnail)
        console.log(ctx.image)
    }, [])

    const image = (ctx.image) ? ctx.image : item.thumbnail

    return (
        <Box m='5'>
        <VStack space={4} alignItems="center">
            <Text fontSize={"3xl"}>{item.title}</Text>
            <Center w="100%" h={200} rounded="md" shadow={3} >
                <Image source={{ uri:image}}
                                style = {DetailStyle.CoverImage.Image}/>
            </Center>
            <ImageList item={item.images}/>
            <Text >{item.description}</Text>
            <Button text= {'Bestel'} action={updateCart}/>
        </VStack>
      </Box>
        // <View style={ DetailStyle.View }>
        //     <View style={ DetailStyle.ContentView}>

        //     </View>
        //     <View style= { DetailStyle.ButtonView}>
        //         <Button text= {'Bestel'} action={updateCart} />
        //     </View>

        // </View>

    )
}

export default Detail