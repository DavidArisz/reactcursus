import React from 'react'
import Detail from './Detail'

const DetailMeta = {
    title: "atoms/Detail",
    component: Detail,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "Detail-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <Detail {...args} />

export const DefaultDetail = Template.bind({})
DefaultDetail.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default DetailMeta