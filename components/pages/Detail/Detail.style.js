/// TODO: add general style(s)
/// import Style from './path/to/general/style

const DetailStyle = {
    View: {
        padding: 10,
        flex: 1,
        display: 'flex'
    },
    ContentView: {
        flex: 5,
        backgroundColor: 'orange'
    },
    ButtonView: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    CoverImage: {
        flex: 1,
        Image: {
            width: "100%",
            height: "100%",
            resizeMode: "cover",
            borderRadius: 10,
        }
    },
}

export default DetailStyle