import { useState, useEffect, useContext } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import FormStyle from './Form.style'
import { Button, InputField } from '../../atoms'

import AppContext from '../../../context/AppContext'
import { submitOrder } from '../../../lib/API'

/// TODO: Define & Destructure props
const Form = ({ navigation }) => {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')

    const ctx = useContext(AppContext)

    const setOrder = () => {
        const order = {
            name,
            email,
            order: ctx.shoppingCart
        }

        submitOrder(order)
            .then( res => console.warn( res))


    }

    return(
        <View style={ FormStyle.View }>
            <Text>Check Out</Text>

            <InputField label={'Name'} handler={setName}/>
            <InputField label={'Email'} handler={setEmail}/>

            <Button text={'Submit'} action={ setOrder} />

        </View>
    )

}

/// TODO: add more utility classes if needed!

/// TODO: Adjust and extend!
const sizes = [
    "small",
    "medium",
    "large"
]

const exceptionClasses = [
    "button",
    "regular"
]

Form.propTypes = {
    testID: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(exceptionClasses),
    size: PropTypes.oneOf(sizes),
}

export default Form