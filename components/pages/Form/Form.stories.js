import React from 'react'
import Form from './Form'

const FormMeta = {
    title: "atoms/Form",
    component: Form,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "Form-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <Form {...args} />

export const DefaultForm = Template.bind({})
DefaultForm.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default FormMeta