import { useState, useEffect, React } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import HomeStyle from './Home.style'

import { ProductList } from '../../organisms'
import { Button } from 'native-base';

/// TODO: Define & Destructure props
const Home = ({ navigation}) => {

    return(
        <View style={ HomeStyle.View }>
            <ProductList type = {'product'}/>
        </View>
    )

}

export default Home