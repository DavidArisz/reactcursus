import React from 'react'
import Home from './Home'

const HomeMeta = {
    title: "atoms/Home",
    component: Home,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "Home-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <Home {...args} />

export const DefaultHome = Template.bind({})
DefaultHome.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default HomeMeta