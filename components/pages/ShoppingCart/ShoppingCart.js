import { useState, useEffect, useContext } from 'react'
import { View, Text } from 'react-native'
import ShoppingCartStyle from './ShoppingCart.style'
import { ProductList } from '../../organisms'
import AppContext from '../../../context/AppContext'
import { Button } from '../../atoms'

/// TODO: Define & Destructure props
const ShoppingCart = ({ navigation}) => {

    const [total, setTotal] = useState(0)
    const ctx = useContext(AppContext)

    const checkOut = () => {
        navigation.navigate('Form')
    }

    useEffect( () => {
        const orderTotal = ctx.shoppingCart.reduce( (total, item) => total + item.price, 0)
        setTotal(orderTotal)
    }), [ctx]

    return(
        <View style={ ShoppingCartStyle.View }>
            <View style = { ShoppingCartStyle.ContentView}>
                <ProductList type={'shopping'}/>
            </View>

            <View style={ ShoppingCartStyle.TotalView}>
                <Text style={ ShoppingCartStyle.TotalView.PriceText}>
                    Total: { total }</Text>
            </View>

            <View style= { ShoppingCartStyle.ButtonView}>
                <Button text= {'Checkout'} action={checkOut} />
            </View>
        </View>
    )
}

export default ShoppingCart