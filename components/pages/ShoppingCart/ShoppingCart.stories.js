import React from 'react'
import ShoppingCart from './ShoppingCart'

const ShoppingCartMeta = {
    title: "atoms/ShoppingCart",
    component: ShoppingCart,
    argTypes: {
        testID: { table: { disable: true } },
        size: { name: "Size" },
        type: { name: "Type"},
        text: { name: "Text" }
    }
}

const testID = "ShoppingCart-" + Math.floor(Math.random() * 90000) + 10000
const Template = (args) => <ShoppingCart {...args} />

export const DefaultShoppingCart = Template.bind({})
DefaultShoppingCart.args = {
    testID: testID,
    text: "Create a story",
    size: "medium",
    type: "regular"
}

export default ShoppingCartMeta