/// TODO: add general style(s)
/// import Style from './path/to/general/style

const ShoppingCartStyle = {
    View: {
        padding: 10,
        flex: 1,
        display: 'flex',
    },
    ContentView: {
        flex: 5,
    },
    TotalView: {
        padding: 10,
        flex: 1,
        display: 'flex',
        alignItems: 'flex-end',
        justifyContent: 'center',
        PriceText: {
            fontSize: 20,
            fontWeight: 'bold',
            color: "#000000-"
        }
    },
    ButtonView: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }
}

export default ShoppingCartStyle