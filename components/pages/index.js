
export { default as Home } from './Home'
export { default as Detail } from './Detail'
export { default as ShoppingCart } from './ShoppingCart'
export { default as Form } from './Form'