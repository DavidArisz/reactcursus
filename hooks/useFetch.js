import { useState, useEffect } from "react";

const useFetch = (url) => {
    const [data, setData] = useState()
    const [isLoaded, setLoaded] = useState(false)

    useEffect( () => {
        fetch(url)
        .then(res => res.json())
        .then(result => {
            setData(result)
            setLoaded(true)
        })
    }, [url] )

    return {data, isLoaded}
}

export default useFetch