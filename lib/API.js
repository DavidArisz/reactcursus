import config from "../config/config"

const submitOrder = (order) => new Promise ( (resolve, reject) => {
    
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(order)
    }

    fetch(config.orderUrl, requestOptions)
        .then(response => response.json())
        .then(data => {
            console.warn(true)
            resolve(true)
        })
        .catch(err => reject(err))

})

export { submitOrder }